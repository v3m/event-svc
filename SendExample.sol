// SPDX-License-Identifier: MIT

pragma solidity >=0.4.22 <0.8.0;

contract SendExample {


   event DataSended(address sender, int val, string str);

   function SendData(int val,  string calldata str) external {
         emit DataSended(msg.sender, val, str);
  }
}
