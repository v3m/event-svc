module event-svc

go 1.14

require (
	github.com/ethereum/go-ethereum v1.10.10
	github.com/jessevdk/go-flags v1.5.0
	github.com/sirupsen/logrus v1.8.1
)
