// event-svc project main.go
package main

import (
	"context"
	"os"
	"os/signal"

	"event-svc/sendexample"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/jessevdk/go-flags"
	log "github.com/sirupsen/logrus"
)

// Options
type Options struct {
	Addr    string `short:"w" long:"ws" env:"WEBSOCKET_RPC_ADDR" description:"WS-RPC server addr" default:"ws://127.0.0.1:8546" required:"yes"`
	Address string `short:"a" long:"addr" description:"Contract address" required:"yes"`
}

// opts
var opts Options

// run
func run(ctx context.Context, opts Options) {
	client, err := ethclient.DialContext(ctx, opts.Addr)
	if err != nil {
		log.WithError(err).WithField("WS addr", opts.Addr).Error("Failed to dial")
		return
	}

	addr := common.HexToAddress(opts.Address)
	se, err := sendexample.NewSendExample(addr, client)
	if err != nil {
		log.WithError(err).Error("failed to bind")
		return
	}

	filteropts := bind.FilterOpts{}
	if filter, err := se.FilterDataSended(&filteropts); err != nil {
		log.WithError(err).Error("failed to filter logs")
	} else if filter != nil {
		for filter.Next() {
			if e := filter.Event; e != nil {
				log.Infof("val %s str %s", e.Val.String(), e.Str)
			}
		}
	}

	callopts := bind.WatchOpts{Context: ctx}
	sink := make(chan *sendexample.SendExampleDataSended)
	sub, err := se.SendExampleFilterer.WatchDataSended(&callopts, sink)
	if err != nil {
		log.WithError(err).Error("failed to subscribe")
		return
	}
	defer sub.Unsubscribe()

log_loop:
	for {
		select {
		case e := <-sub.Err():
			log.WithError(e).Error("filter logs error")
			break log_loop
		case e := <-sink:
			log.Infof("val %s str %s", e.Val.String(), e.Str)

		case <-ctx.Done():
			log.Println("interrupt service")
			break log_loop
		}
	}
}

// main
func main() {
	log.SetLevel(log.InfoLevel)
	log.Info("start event-svc")

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)
	ctx, cancel := context.WithCancel(context.Background())
	go func() {
		<-interrupt
		cancel()
	}()

	if _, err := flags.Parse(&opts); err == nil {
		run(ctx, opts)
	}
}
