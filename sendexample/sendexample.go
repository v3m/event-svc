// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package sendexample

import (
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// SendExampleABI is the input ABI used to generate the binding from.
const SendExampleABI = "[{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"sender\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"int256\",\"name\":\"val\",\"type\":\"int256\"},{\"indexed\":false,\"internalType\":\"string\",\"name\":\"str\",\"type\":\"string\"}],\"name\":\"DataSended\",\"type\":\"event\"},{\"constant\":false,\"inputs\":[{\"internalType\":\"int256\",\"name\":\"val\",\"type\":\"int256\"},{\"internalType\":\"string\",\"name\":\"str\",\"type\":\"string\"}],\"name\":\"SendData\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]"

// SendExampleFuncSigs maps the 4-byte function signature to its string representation.
var SendExampleFuncSigs = map[string]string{
	"0a39f7ca": "SendData(int256,string)",
}

// SendExampleBin is the compiled bytecode used for deploying new contracts.
var SendExampleBin = "0x608060405234801561001057600080fd5b50610165806100206000396000f3fe608060405234801561001057600080fd5b506004361061002b5760003560e01c80630a39f7ca14610030575b600080fd5b6100a76004803603604081101561004657600080fd5b8135919081019060408101602082013564010000000081111561006857600080fd5b82018360208201111561007a57600080fd5b8035906020019184600183028401116401000000008311171561009c57600080fd5b5090925090506100a9565b005b7f3baecef2945d2a63d638dd95574bf28b05da688ade34fe338decca129a5abf083384848460405180856001600160a01b03166001600160a01b03168152602001848152602001806020018281038252848482818152602001925080828437600083820152604051601f909101601f191690920182900397509095505050505050a150505056fea265627a7a7231582027dc4dfd264c19ac6b20d2a10954121f5b0acb2852a76c0bf324a1426c231a1d64736f6c63430005100032"

// DeploySendExample deploys a new Ethereum contract, binding an instance of SendExample to it.
func DeploySendExample(auth *bind.TransactOpts, backend bind.ContractBackend) (common.Address, *types.Transaction, *SendExample, error) {
	parsed, err := abi.JSON(strings.NewReader(SendExampleABI))
	if err != nil {
		return common.Address{}, nil, nil, err
	}

	address, tx, contract, err := bind.DeployContract(auth, parsed, common.FromHex(SendExampleBin), backend)
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	return address, tx, &SendExample{SendExampleCaller: SendExampleCaller{contract: contract}, SendExampleTransactor: SendExampleTransactor{contract: contract}, SendExampleFilterer: SendExampleFilterer{contract: contract}}, nil
}

// SendExample is an auto generated Go binding around an Ethereum contract.
type SendExample struct {
	SendExampleCaller     // Read-only binding to the contract
	SendExampleTransactor // Write-only binding to the contract
	SendExampleFilterer   // Log filterer for contract events
}

// SendExampleCaller is an auto generated read-only Go binding around an Ethereum contract.
type SendExampleCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// SendExampleTransactor is an auto generated write-only Go binding around an Ethereum contract.
type SendExampleTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// SendExampleFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type SendExampleFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// SendExampleSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type SendExampleSession struct {
	Contract     *SendExample      // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// SendExampleCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type SendExampleCallerSession struct {
	Contract *SendExampleCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts      // Call options to use throughout this session
}

// SendExampleTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type SendExampleTransactorSession struct {
	Contract     *SendExampleTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts      // Transaction auth options to use throughout this session
}

// SendExampleRaw is an auto generated low-level Go binding around an Ethereum contract.
type SendExampleRaw struct {
	Contract *SendExample // Generic contract binding to access the raw methods on
}

// SendExampleCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type SendExampleCallerRaw struct {
	Contract *SendExampleCaller // Generic read-only contract binding to access the raw methods on
}

// SendExampleTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type SendExampleTransactorRaw struct {
	Contract *SendExampleTransactor // Generic write-only contract binding to access the raw methods on
}

// NewSendExample creates a new instance of SendExample, bound to a specific deployed contract.
func NewSendExample(address common.Address, backend bind.ContractBackend) (*SendExample, error) {
	contract, err := bindSendExample(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &SendExample{SendExampleCaller: SendExampleCaller{contract: contract}, SendExampleTransactor: SendExampleTransactor{contract: contract}, SendExampleFilterer: SendExampleFilterer{contract: contract}}, nil
}

// NewSendExampleCaller creates a new read-only instance of SendExample, bound to a specific deployed contract.
func NewSendExampleCaller(address common.Address, caller bind.ContractCaller) (*SendExampleCaller, error) {
	contract, err := bindSendExample(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &SendExampleCaller{contract: contract}, nil
}

// NewSendExampleTransactor creates a new write-only instance of SendExample, bound to a specific deployed contract.
func NewSendExampleTransactor(address common.Address, transactor bind.ContractTransactor) (*SendExampleTransactor, error) {
	contract, err := bindSendExample(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &SendExampleTransactor{contract: contract}, nil
}

// NewSendExampleFilterer creates a new log filterer instance of SendExample, bound to a specific deployed contract.
func NewSendExampleFilterer(address common.Address, filterer bind.ContractFilterer) (*SendExampleFilterer, error) {
	contract, err := bindSendExample(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &SendExampleFilterer{contract: contract}, nil
}

// bindSendExample binds a generic wrapper to an already deployed contract.
func bindSendExample(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(SendExampleABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_SendExample *SendExampleRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _SendExample.Contract.SendExampleCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_SendExample *SendExampleRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _SendExample.Contract.SendExampleTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_SendExample *SendExampleRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _SendExample.Contract.SendExampleTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_SendExample *SendExampleCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _SendExample.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_SendExample *SendExampleTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _SendExample.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_SendExample *SendExampleTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _SendExample.Contract.contract.Transact(opts, method, params...)
}

// SendData is a paid mutator transaction binding the contract method 0x0a39f7ca.
//
// Solidity: function SendData(int256 val, string str) returns()
func (_SendExample *SendExampleTransactor) SendData(opts *bind.TransactOpts, val *big.Int, str string) (*types.Transaction, error) {
	return _SendExample.contract.Transact(opts, "SendData", val, str)
}

// SendData is a paid mutator transaction binding the contract method 0x0a39f7ca.
//
// Solidity: function SendData(int256 val, string str) returns()
func (_SendExample *SendExampleSession) SendData(val *big.Int, str string) (*types.Transaction, error) {
	return _SendExample.Contract.SendData(&_SendExample.TransactOpts, val, str)
}

// SendData is a paid mutator transaction binding the contract method 0x0a39f7ca.
//
// Solidity: function SendData(int256 val, string str) returns()
func (_SendExample *SendExampleTransactorSession) SendData(val *big.Int, str string) (*types.Transaction, error) {
	return _SendExample.Contract.SendData(&_SendExample.TransactOpts, val, str)
}

// SendExampleDataSendedIterator is returned from FilterDataSended and is used to iterate over the raw logs and unpacked data for DataSended events raised by the SendExample contract.
type SendExampleDataSendedIterator struct {
	Event *SendExampleDataSended // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *SendExampleDataSendedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(SendExampleDataSended)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(SendExampleDataSended)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *SendExampleDataSendedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *SendExampleDataSendedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// SendExampleDataSended represents a DataSended event raised by the SendExample contract.
type SendExampleDataSended struct {
	Sender common.Address
	Val    *big.Int
	Str    string
	Raw    types.Log // Blockchain specific contextual infos
}

// FilterDataSended is a free log retrieval operation binding the contract event 0x3baecef2945d2a63d638dd95574bf28b05da688ade34fe338decca129a5abf08.
//
// Solidity: event DataSended(address sender, int256 val, string str)
func (_SendExample *SendExampleFilterer) FilterDataSended(opts *bind.FilterOpts) (*SendExampleDataSendedIterator, error) {

	logs, sub, err := _SendExample.contract.FilterLogs(opts, "DataSended")
	if err != nil {
		return nil, err
	}
	return &SendExampleDataSendedIterator{contract: _SendExample.contract, event: "DataSended", logs: logs, sub: sub}, nil
}

// WatchDataSended is a free log subscription operation binding the contract event 0x3baecef2945d2a63d638dd95574bf28b05da688ade34fe338decca129a5abf08.
//
// Solidity: event DataSended(address sender, int256 val, string str)
func (_SendExample *SendExampleFilterer) WatchDataSended(opts *bind.WatchOpts, sink chan<- *SendExampleDataSended) (event.Subscription, error) {

	logs, sub, err := _SendExample.contract.WatchLogs(opts, "DataSended")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(SendExampleDataSended)
				if err := _SendExample.contract.UnpackLog(event, "DataSended", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseDataSended is a log parse operation binding the contract event 0x3baecef2945d2a63d638dd95574bf28b05da688ade34fe338decca129a5abf08.
//
// Solidity: event DataSended(address sender, int256 val, string str)
func (_SendExample *SendExampleFilterer) ParseDataSended(log types.Log) (*SendExampleDataSended, error) {
	event := new(SendExampleDataSended)
	if err := _SendExample.contract.UnpackLog(event, "DataSended", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
